import os
import sys
import errno
import shutil
import logging
import time
import ImageExifEditor

# Const declaration
SOURCE_DIRECTORY = os.getcwd()

# If script is called directly
if __name__ == '__main__':
    # Script execution start time
    script_start_time = time.time()

    amount_file_sorted = 0

    for root, directories, files in os.walk(SOURCE_DIRECTORY):
        for file_name in files:
            # if is a photo of (jp{e}?g) type
            # PNG support when the piexif supports it
            if file_name.lower().endswith(('jpg', 'jpeg')):
                # Current image path
                image_path_current = os.path.join(root, file_name)
                # Load Image metadata
                iee = ImageExifEditor.ImageExifEditor(image_path_current)
                # Try to fetch image datetime and sort it in the relevant directory
                try:
                    image_datetime = iee.get_image_datetime()
                    image_date = image_datetime.strftime('%Y-%m-%d')
                    directory_date_path = os.path.join(root, image_date)

                    if not os.path.basename(os.path.dirname(image_path_current)) == image_date:
                        # Create directory if non existent:
                        try:
                            os.makedirs(directory_date_path)
                        except OSError as directory_exception:
                            if directory_exception.errno != errno.EEXIST:
                                raise
                        # Move file to wanted date directory:
                        shutil.move(image_path_current, directory_date_path + os.sep + file_name)

                except KeyError:
                    logging.warning('No datetime for image at : ' + image_path_current)
                else:
                    # Progress bar
                    amount_file_sorted += 1
                    sys.stdout.write("\r %d photos sorted" % amount_file_sorted)
                    sys.stdout.flush()

    # Script execution time
    script_execution_time = time.time() - script_start_time
    script_execution_time_string = time.strftime("%H:%M:%S", time.gmtime(script_execution_time))
    sys.stdout.write("\r Total execution time : " + script_execution_time_string + " for %d photos sorted." % amount_file_sorted)
    sys.stdout.flush()
