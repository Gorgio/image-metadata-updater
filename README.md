# Image Metadata Updater

A side project in Python so I can fix the datetime Exif tags on my photos and automatically sort them in corresponding
folders.

# Requirement
Piexif library:
https://github.com/hMatoba/Piexif

Pillow (PIL):
https://github.com/python-pillow/Pillow

# Usage
## sort.py
Just call the python script in the folder you want to sort and it will do the job for you.

## update-datetime.py
Not usable out of the box for the moment.
You need to manually edit the code to fix your datetime to bring it to UTC and then you can use a Timezone parameter to
handle DST (Daylight Saving Time) or just manually set up how much you want to shift the datetime.

# Environment
Tested on Python 3.6
 