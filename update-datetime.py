import os
import sys
import logging
import time
from datetime import datetime, timedelta
import pytz
import ImageExifEditor

# Const declaration
CURRENT_WORK_DIRECTORY = os.getcwd()


def convert_utc_to_timezone(datetime_to_convert: datetime, timezone: str):
    utc_time = pytz.utc.localize(datetime_to_convert)
    new_timezone = pytz.timezone(timezone)

    # Returns datetime in the new timezone
    datetime_in_new_timezone = utc_time.astimezone(new_timezone)
    return datetime_in_new_timezone


# If script is called directly
if __name__ == '__main__':
    # Time offset
    # Old photos are UTC + 02h11m
    # -11 minutes for old photos
    offset = timedelta(hours=-2, minutes=-11)

    # JST = 'Asia/Tokyo'
    # Australia NSW : Australia/Sydney
    # Australia South Australia: Australia/Adelaide
    # Australia Cairns (QLD): UTC +10
    # Europe/London

    # Script execution start time
    script_start_time = time.time()

    amount_file_edit = 0

    for root, directories, file_names in os.walk(CURRENT_WORK_DIRECTORY):
        for file_name in file_names:
            # if is a photo of (jp{e}?g) type
            # PNG support when the piexif supports it
            if file_name.lower().endswith(('jpg', 'jpeg')):
                # Current image path
                image_path_current = os.path.join(root, file_name)
                iee = ImageExifEditor.ImageExifEditor(image_path_current)
                try:
                    image_datetime = iee.get_image_datetime()
                    # Apply an offset to the image time if one has been given
                    if offset is not None:
                        image_datetime += offset

                    image_datetime_new = convert_utc_to_timezone(image_datetime, 'Europe/London')
                    # image_datetime_new = image_datetime
                    iee.set_image_datetime(image_datetime_new)
                except KeyError:
                    logging.warning('No datetime for image at : ' + image_path_current)
                else:
                    iee.set_copyright('George Peter Banyard')
                    iee.image_rotate_from_exif()
                    # Progress bar
                    amount_file_edit += 1
                    sys.stdout.write("\r %d photos modified" % amount_file_edit)
                    sys.stdout.flush()

    # Script execution time
    script_execution_time = time.time() - script_start_time
    script_execution_time_string = time.strftime("%H:%M:%S", time.gmtime(script_execution_time))
    print("Total execution time : " + script_execution_time_string + " for %d photos edited." % amount_file_edit)
