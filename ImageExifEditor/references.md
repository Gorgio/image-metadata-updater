# Exif Reference 

From http://www.cipa.jp/std/documents/e/DC-008-2012_E.pdf page 39

## DateTime 

The date and time of image creation. In this standard it is the date and time the file was changed. The 
format  is  ``YYYY:MM:DD  HH:MM:SS``  with  time  shown  in  24-hour  format,  and  the  date  and  time  
separated  by  one  blank  character  [20.H].  When  the  date  and  time  are  unknown,  all  the  character  
spaces  except  colons  (```:```)  should  be  filled  with  blank  characters,  or  else  the  Interoperability  field  
should  be  filled  with  blank  characters.  The  character  string  length  is  20  Bytes  including  NULL  for  
termination. When the field is left blank, it is treated as unknown.   
Tag             =     306     (132.H)     
Type            =     ASCII   
Count           =     20  
Default         =     None

## Artist

## Copyright