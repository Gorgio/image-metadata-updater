# Exif dict IFD order and constants
class IFDs:
    IFD_0 = '0th'
    IFD_EXIF = 'Exif'
    IFD_GPS = 'GOS'
    IFD_INTEROP = 'Interop'
    IFD_1 = '1st'
    IFD_THUMBNAIL = 'thumbnail'
