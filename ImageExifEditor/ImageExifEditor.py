import piexif
from PIL import Image
from datetime import datetime
from ImageExifEditor.IFDs import IFDs


class ImageExifEditor:
    # Exif tags constants
    EXIF_DATETIME_ENCODING = 'ASCII'
    EXIF_DATETIME_FORMAT = '%Y:%m:%d %H:%M:%S'
    EXIF_ARTIST_ENCODING = 'ASCII'
    EXIF_COPYRIGHT_ENCODING = 'ASCII'

    '''
    Initialize ImageExifEditor via the image path provided
    '''
    def __init__(self, image_path: str):
        self.ImagePath = image_path
        self.ExifDict = piexif.load(self.ImagePath)

    '''
    Generates the whole list of tags for the 0th, Exif, GPS and 1st IFD
    
    Returns (dict) exif_tags
    '''
    def image_get_exif_tags(self):
        exif_tags = {}
        for ifd in (IFDs.IFD_0, IFDs.IFD_EXIF, IFDs.IFD_GPS, IFDs.IFD_1):
            for tag in self.ExifDict[ifd]:
                exif_tags[piexif.TAGS[ifd][tag]["name"]] = self.ExifDict[ifd][tag]

        return exif_tags

    '''
    Private method to update the Exif tags of the photos
    
    Returns (void)
    '''
    def _update_exif_tags(self, exif_dict: dict):
        if IFDs.IFD_0 in exif_dict:
            self.ExifDict[IFDs.IFD_0].update(exif_dict[IFDs.IFD_0])

        if IFDs.IFD_EXIF in exif_dict:
            self.ExifDict[IFDs.IFD_EXIF].update(exif_dict[IFDs.IFD_EXIF])

        if IFDs.IFD_GPS in exif_dict:
            self.ExifDict[IFDs.IFD_GPS].update(exif_dict[IFDs.IFD_GPS])

        if IFDs.IFD_INTEROP in exif_dict:
            self.ExifDict[IFDs.IFD_INTEROP].update(exif_dict[IFDs.IFD_INTEROP])

        if IFDs.IFD_1 in exif_dict:
            self.ExifDict[IFDs.IFD_1].update(exif_dict[IFDs.IFD_1])

        if IFDs.IFD_THUMBNAIL in exif_dict:
            self.ExifDict[IFDs.IFD_THUMBNAIL].update(exif_dict[IFDs.IFD_THUMBNAIL])

    '''
    Get the most appropriate datetime from the image depending on the available Exif tags.
    
    Returns (datetime.datetime)
    Raises KeyError
    '''
    def get_image_datetime(self):
        error_code = 0

        if piexif.ImageIFD.DateTime in self.ExifDict[IFDs.IFD_0]:
            exif_datetime = self.ExifDict[IFDs.IFD_0][piexif.ImageIFD.DateTime]
        else:
            exif_datetime = None
            error_code += 1

        if piexif.ExifIFD.DateTimeOriginal in self.ExifDict[IFDs.IFD_EXIF]:
            exif_datetime_original = self.ExifDict[IFDs.IFD_EXIF][piexif.ExifIFD.DateTimeOriginal]
        else:
            exif_datetime_original = None
            error_code += 2

        if piexif.ExifIFD.DateTimeDigitized in self.ExifDict[IFDs.IFD_EXIF]:
            exif_datetime_digitized = self.ExifDict[IFDs.IFD_EXIF][piexif.ExifIFD.DateTimeDigitized]
        else:
            exif_datetime_digitized = None
            error_code += 4

        if error_code == 7:
            raise KeyError('No datetime value present in EXIF tags of ' + self.ImagePath)

        elif exif_datetime == exif_datetime_digitized == exif_datetime_original:
            date_to_return = exif_datetime

        elif exif_datetime is not None:
            date_to_return = exif_datetime

        elif exif_datetime_original == exif_datetime_digitized is not None:
            date_to_return = exif_datetime_original

        else:
            date_to_return = exif_datetime_digitized

        return datetime.strptime(date_to_return.decode(self.EXIF_DATETIME_ENCODING), self.EXIF_DATETIME_FORMAT)

    '''
    Sets the new date time to all relevant (i.e. DateTime, DateTimeOriginal, DateTimeDigitized) Exif tags while
    preserving other Exif tags.
    
    Returns (void)
    '''
    def set_image_datetime(self, image_datetime: datetime):
        datetime_new = image_datetime.strftime(self.EXIF_DATETIME_FORMAT).encode(self.EXIF_DATETIME_ENCODING)
        exif_ifd = {
            piexif.ExifIFD.DateTimeOriginal: datetime_new,
            piexif.ExifIFD.DateTimeDigitized: datetime_new
        }
        zeroth_ifd = {piexif.ImageIFD.DateTime: datetime_new}
        self._update_exif_tags({IFDs.IFD_0: zeroth_ifd, IFDs.IFD_EXIF: exif_ifd})

    # Todo get_copyright
    '''
    Sets the new copyright to all relevant Exif tags (i.e. Artist and Copyright) while preserving other Exif tags.
    
    Returns (void)
    '''
    def set_copyright(self, copyright_string: str):
        zeroth_ifd = {
            piexif.ImageIFD.Copyright: copyright_string.encode(self.EXIF_COPYRIGHT_ENCODING),
            piexif.ImageIFD.Artist: copyright_string.encode(self.EXIF_ARTIST_ENCODING)
        }
        self._update_exif_tags({IFDs.IFD_0: zeroth_ifd})

    '''
    Set exif tags and saves the image
    
    Returns (void)
    '''
    def set_exif_tags(self):
        exif_bytes = piexif.dump(self.ExifDict)
        image = Image.open(self.ImagePath)
        image.save(self.ImagePath, exif=exif_bytes)

    '''
    Rotates image given the information provided in the 0th Exif
    
    Heavily inspired from piexif documentation : http://piexif.readthedocs.io/en/latest/sample.html
    
    Note: This saves the images at the end with any new Exif tags (i.e no need to apply set_exif_tags before/after)
    '''
    def image_rotate_from_exif(self):
        if piexif.ImageIFD.Orientation in self.ExifDict[IFDs.IFD_0]:
            orientation = self.ExifDict[IFDs.IFD_0].pop(piexif.ImageIFD.Orientation)
            exif_bytes = piexif.dump(self.ExifDict)

            image = Image.open(self.ImagePath)
            if orientation == 2:
                image = image.transpose(Image.FLIP_LEFT_RIGHT)
            elif orientation == 3:
                image = image.rotate(180)
            elif orientation == 4:
                image = image.rotate(180).transpose(Image.FLIP_LEFT_RIGHT)
            elif orientation == 5:
                image = image.rotate(-90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
            elif orientation == 6:
                image = image.rotate(-90, expand=True)
            elif orientation == 7:
                image = image.rotate(90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
            elif orientation == 8:
                image = image.rotate(90, expand=True)

            image.save(self.ImagePath, exif=exif_bytes)
        else:
            self.set_exif_tags()
